

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// OpenGL / Cg shader demo - uses excerpts from here:
// http://bobobobo.wordpress.com/2008/02/11/opengl-in-a-proper-windows-app-no-glut/
// Feel free to adapt this for what you need, but please leave these comments in.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

/*

IMPORTANT - before this demo will work, you need to do a number of things:
1. if you are NOT on a university games lab PC, you will need to download and install "cg toolkit" from the Nvidia website.
2. create a visual studio "empty project" and add this file as a source file as usual. don't create a console application!
3. in the same folder as the visual studio SOLUTION (the .sln file), create a folder called "shaders"
4. paste the two shader files that came with this demo into the "shaders" folder.
5. in the solution explorer, right-click on the project and select "properties"
6. under "C++ -> General", click "additional include directories" and use the drop-down to select "edit".
7. add this additional include directory to the list:	 C:\Program Files (x86)\NVIDIA Corporation\Cg\include
8. then under "Linker -> Input -> Additional Dependencies", copy/paste the following 4 lines into the list:


opengl32.lib
glu32.lib
C:/Program Files (x86)/NVIDIA Corporation/Cg/lib/cg.lib
C:/Program Files (x86)/NVIDIA Corporation/Cg/lib/cggl.lib


(please note that if you installed cg toolkit somewhere other than C:/Program Files, you will need to edit all cg filepaths above to reflect this!)


ONCE YOU HAVE GOT THIS FAR, CHECK THE SAMPLE BUILDS AND RUNS,
THEN YOU NEED TO READ THROUGH THE REST OF THE COMMENTS IN THIS FILE TO UNDERSTAND WHAT IS GOING ON!

*/


#pragma once

// the usual includes...
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

// these extra includes are needed by the loadShader function (don't worry about this for now)...
#include <iostream>
#include <fstream>
#include <string>

// these extra includes are needed to work with Cg shaders...
#include "CG/Cg.h"		// cg shader language base functions
#include "CG/CgGL.h"	// cg shader language opengl-specific functions

using namespace std;

// vertex array
#define GRIDWIDTH 100
#define GRIDHEIGHT 100



// note how we don't have a color array any more.
// we are going to do our color the "proper" way by using the fragment shader.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


class Game
{
public:

	// this is a variable for the rotation of the triangle...
	float rotation;

	float hexArray[24];
	float angle = 0.0f;
	int playerX = 1;
	int playerY = 1;
	float radius;

	float mapPositionX = 0.0f;
	float mapPositionY = 0.0f;
	float hexSizeX;
	float hexSizeY;

	bool keyDown;

	// these are the variables we need to work with Cg shaders...

	CGcontext context;			// this will store our Cg context

	CGprofile vShaderProfile;	// vertex shader profile to use
	CGprofile fShaderProfile;	// fragment shader profile to use
	CGprogram vShaderProgram;	// vertex shader program to use
	CGprogram fShaderProgram;	// fragment shader program to use

	CGparameter mvMatrix;		// will be used to create handle to the modelview matrix in the vertex shader 
	CGparameter fragColor;		// will be used to create handle to the color in the fragment shader 

	// function prototypes...
	Game();		// constructor
	~Game();	// destructor
	char* loadShader(string filePath);	// for converting text files into shader-compatible strings
	void update(void);	// function to do update logic
	void draw(void);	// function to do drawing logic
	// note how we have put the update and draw functions inside the game class, to make it easier later on.	
};

// the rest of the app is pretty much exactly the same as the OpenGL examples we have seen so far.
// you can skip ahead to just above the main game loop...

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// function prototypes:
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// In a C++ Windows app, the starting point is WinMain() rather than _tmain() or main().
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	// some basic numbers to hold the position and size of the window
#define WIDTH		1980
#define HEIGHT		1080
#define TOPLEFTX	50
#define TOPLEFTY	50

// this bit creates a window class, basically a template for the window we will make later, so we can do more windows that look the same.
	WNDCLASS myWindowClass;
	myWindowClass.cbClsExtra = 0;											// no idea
	myWindowClass.cbWndExtra = 0;											// no idea
	myWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	// background fill black
	myWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);					// arrow cursor       
	myWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);				// type of icon to use (default app icon)
	myWindowClass.hInstance = hInstance;									// window instance name (given by the OS when the window is created)   
	myWindowClass.lpfnWndProc = WndProc;									// window callback function - this is our function below that is called whenever something happens
	myWindowClass.lpszClassName = TEXT("my window class name");				// our new window class name
	myWindowClass.lpszMenuName = 0;											// window menu name (0 = default menu?) 
	myWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				// redraw if the window is resized horizontally or vertically, allow different context for each window instance

	// Register that class with the Windows OS..
	RegisterClass(&myWindowClass);


	// create a rect structure to hold the dimensions of our window
	RECT rect;
	SetRect(&rect, TOPLEFTX,				// the top-left corner x-coordinate
		TOPLEFTY,				// the top-left corner y-coordinate
		TOPLEFTX + WIDTH,		// far right
		TOPLEFTY + HEIGHT);	// far left


// adjust the window, no idea why.
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);



	// call CreateWindow to create the window
	HWND myWindow = CreateWindow(TEXT("my window class name"),		// window class to use - in this case the one we created a minute ago
		TEXT("OpenGL with Shaders"),				// window title
		WS_OVERLAPPEDWINDOW,						// ??
		TOPLEFTX, TOPLEFTY,						// x, y
		TOPLEFTX + WIDTH, TOPLEFTY + HEIGHT,		// width and height
		NULL, NULL,								// ??
		hInstance, NULL);							// ??


// check to see that the window created okay
	if (myWindow == NULL)
	{
		FatalAppExit(NULL, TEXT("CreateWindow() failed!"));
	}

	// if so, show it
	ShowWindow(myWindow, iCmdShow);


	// get a device context from the window
	HDC myDeviceContext = GetDC(myWindow);


	// we create a pixel format descriptor, to describe our desired pixel format. 
	// we set all of the fields to 0 before we do anything else
	// this is because PIXELFORMATDESCRIPTOR has loads of fields that we won't use
	PIXELFORMATDESCRIPTOR myPfd = { 0 };


	// now set only the fields of the pfd we care about:
	myPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);		// size of the pfd in memory
	myPfd.nVersion = 1;									// always 1

	myPfd.dwFlags = PFD_SUPPORT_OPENGL |				// OpenGL support - not DirectDraw
		PFD_DOUBLEBUFFER |				// double buffering support
		PFD_DRAW_TO_WINDOW;					// draw to the app window, not to a bitmap image (for example)

	myPfd.iPixelType = PFD_TYPE_RGBA;					// red, green, blue, alpha for each pixel
	myPfd.cColorBits = 24;								// 24 bit == 8 bits for red, 8 for green, 8 for blue.
														// This count of color bits EXCLUDES alpha.

	myPfd.cDepthBits = 32;								// 32 bits to measure pixel depth.



	// now we need to choose the closest pixel format to the one we wanted...	
	int chosenPixelFormat = ChoosePixelFormat(myDeviceContext, &myPfd);

	// if windows didnt have a suitable format, 0 would have been returned...
	if (chosenPixelFormat == 0)
	{
		FatalAppExit(NULL, TEXT("ChoosePixelFormat() failed!"));
	}

	// if we get this far it means we've got a valid pixel format
	// so now we need to set the device context up with that format...
	int result = SetPixelFormat(myDeviceContext, chosenPixelFormat, &myPfd);

	// if it failed...
	if (result == NULL)
	{
		FatalAppExit(NULL, TEXT("SetPixelFormat() failed!"));
	}


	// we now need to set up a render context (for opengl) that is compatible with the device context (from windows)...
	HGLRC myRenderContext = wglCreateContext(myDeviceContext);

	// then we connect the two together
	wglMakeCurrent(myDeviceContext, myRenderContext);


	// opengl display setup
	glMatrixMode(GL_PROJECTION);								// select the projection matrix, i.e. the one that controls the "camera"
	glLoadIdentity();											// reset it
	gluPerspective(45.0, (float)WIDTH / (float)HEIGHT, 0.01, 1000);	// set up fov, and near / far clipping planes
	glViewport(0, 0, WIDTH, HEIGHT);							// make the viewport cover the whole window
	glClearColor(0.5, 0, 0, 0);								// set the colour used for clearing the screen



	// some other variables needed for our game...
	MSG msg;								// this will be used to store messages from the operating system
	bool keepPlaying = true;				// whether or not the player wants to keep playing

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	// here we are going to create a game object...
	Game* currentGame = new Game;


	// main game loop starts here!


	// as long as the player hasnt exited the app: 
	while (keepPlaying == true)
	{
		
		// we need to listen out for OS messages.
		// if there is a windows message to process...
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{

			// if the message was a "quit" message...
			if (msg.message == WM_QUIT)
			{
				keepPlaying = false; // we want to quit asap
			}

			else if (msg.message == WM_KEYDOWN)
			{
				if (msg.wParam == 'A')
				{
					//try move function
					if (currentGame->keyDown == false)
					{
						currentGame->keyDown = true;
						currentGame->playerX -= 1;
						currentGame->mapPositionX += (currentGame->hexSizeX);

					}
				}
				else if (msg.wParam == 'D')
				{
					if (currentGame->keyDown == false)
					{
						currentGame->keyDown = true;
						currentGame->playerX += 1;
						currentGame->mapPositionX -= (currentGame->hexSizeX);
					}
				}
				else if (msg.wParam == 'W')
				{
					if (currentGame->keyDown == false)
					{
						currentGame->keyDown = true;
						currentGame->playerY += 1;
						currentGame->mapPositionY -= (currentGame->hexSizeY);
					}
				}
				else if (msg.wParam == 'S')
				{
					if (currentGame->keyDown == false)
					{
						currentGame->keyDown = true;
						currentGame->playerY -= 1;
						currentGame->mapPositionY += (currentGame->hexSizeY);
					}
				}
			}
			else if (msg.message == WM_KEYUP)
			{

				currentGame->keyDown = false;

			}
			// if it was any other type of message (i.e. one we don't care about), process it as normal
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// recall that the update and draw functions are now inside the game class...
		currentGame->update();
		currentGame->draw();
		SwapBuffers(myDeviceContext);

	}
	// end of main game loop

	// the next bit will therefore happen when the player quits the app...

	// free up the memory used by the game and set its pointer to NULL
	delete currentGame;
	currentGame = NULL;

	// UNmake our opengl rendering context (make it 'uncurrent')
	wglMakeCurrent(NULL, NULL);

	// delete the rendering context, we no longer need it.
	wglDeleteContext(myRenderContext);

	// release our window's DC from the window
	ReleaseDC(myWindow, myDeviceContext);

	// end the program
	return msg.wParam;
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// this function is called when any events happen to our window
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
		// if they exited the window...	
	case WM_DESTROY:
		// post a message "quit" message to the main windows loop
		PostQuitMessage(0);
		return 0;
		break;
	}

	// must do this as a default case (i.e. if no other event was handled)...
	return DefWindowProc(hwnd, message, wparam, lparam);
}



// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// here is the game constructor - it sets up the initial rotation of the triangle and gets everything ready to work with shaders.
// you won't need to touch the majority of the shader setup code, until you want to use different shaders for different objects.

Game::Game()
{
	hexArray[0] = 0.0f;
	hexArray[1] = 0.0f;
	hexArray[2] = 0.0f;

	radius = 0.1f;
	float degToRad = 0.01745f;
	int point = 1;

	playerX = 1;
	playerY = 1;

	for (float i = 0.0f; i <= 360; i += 60)
	{
		hexArray[(point * 3) + 0] = radius * (cos(i*degToRad));
		hexArray[(point * 3) + 1] = radius * (sin(i*degToRad));
		hexArray[(point * 3) + 2] = 0.0f;
		point++;
	}
	hexSizeX = radius * 1.5;
	hexSizeY = radius * sqrt(3);

	// initial rotation of the triangle...
	rotation = 0.0f;

	// we need a cg "context", kind of "initialises" the shader system...
	context = cgCreateContext();

	// then we ask the graphics card for the latest shader program "profiles" it supports (basically what "version"), and store these for later...
	vShaderProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
	fShaderProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);

	// then we set cg up to work best for those profiles...
	cgGLSetOptimalOptions(vShaderProfile);
	cgGLSetOptimalOptions(fShaderProfile);

	// then we access our shader text files, process them using the loadShader function, and store their locations in pointers...
	char* myVertexProgram = loadShader("../shaders/topic12VertexShader.cg");
	char* myFragmentProgram = loadShader("../shaders/topic12FragmentShader.cg");



	// then we compile those shader text files into actual shader program code:
	// we have a vertex and fragment shader, so we need two calls to the cgCreateProgram() function.
		// the first argument in each call is the current cg context we are using - in this case, our context was called "context".
		// the second argument, "CG_SOURCE", lets cg know what format we are using for our shader code (and therefore how it should be interpreted by the compiler)
		// then we have a pointer to some vertex/fragment shader source code.
		// then we have the name of our graphics card's best shader profile for that type of shader
		// "main" indicates the name of the starting function inside the shader code (you can call it what you want as long as it matches up)
		// no idea why we put NULL at the end, I'm sure we can find out if need be.		
	vShaderProgram = cgCreateProgram(context, CG_SOURCE, myVertexProgram, vShaderProfile, "main", NULL);
	fShaderProgram = cgCreateProgram(context, CG_SOURCE, myFragmentProgram, fShaderProfile, "main", NULL);




	// once it is compiled, load the shader program code...
	cgGLLoadProgram(vShaderProgram);
	cgGLLoadProgram(fShaderProgram);


	// now we have to create a link between our mvMatrix variable (in the game class), and the mvMatrix variable in the vertex shader...
	mvMatrix = cgGetNamedParameter(vShaderProgram, "mvMatrix");

	// also create a link between the the fragColor variable (in the game class) and the the fragColor variable inside the fragment shader...
	fragColor = cgGetNamedParameter(fShaderProgram, "fragColor");

	// mvMatrix and fragColor (in the game class) are now known as "handles":
	// if we edit either of these variables, the corresponding variables inside the shaders will also change.
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

Game::~Game()
{
	// destroy anything that was allocated for cg...
	cgDestroyParameter(mvMatrix);
	cgDestroyParameter(fragColor);

	cgDestroyProgram(vShaderProgram);
	cgDestroyProgram(fShaderProgram);

	// destroy the context for cg...
	cgDestroyContext(context);
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

void Game::update(void)
{
	angle += 1.0f;
}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

void Game::draw(void)
{
	// normal opengl setup...
	glEnableClientState(GL_VERTEX_ARRAY);
	// (note how we don't need to enable vertex color arrays, as we don't need them)
	glMatrixMode(GL_MODELVIEW);

	// prepare cg to use the necessary shader profiles...
	cgGLEnableProfile(vShaderProfile);
	cgGLEnableProfile(fShaderProfile);

	// bind the shaders we want to use - basically means "select" them - we should do this each frame because may have more than one set of shaders in our game.
	cgGLBindProgram(vShaderProgram);
	cgGLBindProgram(fShaderProgram);

	// select which array to use for the mesh as usual...
	glVertexPointer(3, GL_FLOAT, 0, hexArray);
	// (again, note how we don't need to select a color array.)

	// then the usual transformation code...
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.0f);
	glRotatef(rotation, 0.0, 1.0, 0.0);

	// BUT... before we draw, we just need to do a couple of things...

	// first, we need to copy the current transformation matrix into the vertex shader, using the mvMatrix handle we created earlier...
	cgGLSetStateMatrixParameter(mvMatrix, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);

	


	//width of a hex is 1.5 * the radius
	float width = radius * 1.5f;
	//height of a hex is radius * sqrt(3)
	float height = radius * sqrt(3);
	float z = 0.0f;


	for (int x = 0; x < GRIDHEIGHT; x++)
	{
		float offsetY = 0.0f;
		if (x & 1)
		{
			//x is odd
			offsetY = height / 2;
		}

		for (int y = 0; y < GRIDWIDTH; y++)
		{
			glVertexPointer(3, GL_FLOAT, 0, hexArray);

			glPushMatrix();


			// then, we set the colour of the triangle in the fragment shader, using the fragColor handle we created earlier...
			// (handle name, r, g, b, a)
			cgSetParameter4f(fragColor, 1.0f, 1.0f, 0.0f, 1.0f);
			glTranslatef(x*width + mapPositionX, ((y*height) + offsetY) + mapPositionY, -1.0f);
			if (x == playerX && y == playerY)
			{
				glRotatef(angle, 1.0f, 0.0f, 0.0f);
			}

			// first, we need to copy the current transformation matrix into the vertex shader, using the mvMatrix handle we created earlier...
			cgGLSetStateMatrixParameter(mvMatrix, CG_GL_MODELVIEW_PROJECTION_MATRIX, CG_GL_MATRIX_IDENTITY);
			glDrawArrays(GL_TRIANGLE_FAN, 0, 8);
			glPopMatrix();

		}
	}
	glPopMatrix();

	// remember if we bind/enable something, we should unbind/disable it...
	cgGLUnbindProgram(vShaderProfile);
	cgGLUnbindProgram(fShaderProfile);
	cgGLDisableProfile(vShaderProfile);
	cgGLDisableProfile(fShaderProfile);

	glDisableClientState(GL_VERTEX_ARRAY);

	// and we are done!
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// this function is purely to convert a text file into a string, you are not expected to understand this but feel free to experiment...
char* Game::loadShader(string filePath)
{

	// create an ifstream file with the filename passed to the function
	// the file is an input file (for reading), and it is in binary
	ifstream file(filePath.c_str(), ios::in | ios::binary);

	// Indicate if no file found, it will be NULL if it failed.
	if (!file)
	{
		cout << "shader text file not found" << endl;
		return 0;
	}

	// an integer to hold the size of the shader code file
	int size = 0;

	// Get size of the file by moving to the end of it and seeing how far we've gone
	file.seekg(0, ios::end);
	size = file.tellg();

	// Indicate if file is empty
	if (!size)
	{
		cout << "shader file is empty!" << endl;
	}

	// a container to hold the data, via dynamically-allocated memory
	char* data = 0;

	// allocate memory for data
	data = new char[size + 1];

	// move back to the start of the file
	file.seekg(0, ios::beg);

	// Read file (we already know how much to read)
	file.read(data, size);

	// Null terminate char array so it knows it is the end
	data[size] = '\0';

	return data;
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@